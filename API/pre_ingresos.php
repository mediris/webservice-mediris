<?php 

	header('Content-Type: application/json');
	
	include "../lib/Connections.php";
	include "../lib/Logs.php";
	include "../lib/Config.php";

	$logs = new Logs();

	/* Se comprueba el valor que esta recibiendo desde la url el webservice */
	if($_GET['solicitud'] == 'aprobados'){
		$connect = new Connections('CORREO', 'CORREO', 'idasysi');
		buscar_registros($connect, $logs);
	}else if($_GET['solicitud'] == 'preingresos'){
		$connect = new Connections('CORREO', 'CORREO', 'idasysi');
		preingresos($connect, $_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'reporte'){
		reporte($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'correo'){
		correo($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'ciencias'){
		reporteTrinidadCLC($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'facturacion'){
		$connect = new Connections('CORREO', 'CORREO', 'idasysi');
		facturacion($_GET['localidad'], $connect, $logs);
	}else if($_GET['solicitud'] == 'cedula'){
		cambiarCedula($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'validarimgs'){
		validarImagenPACS($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'solicitud'){
		envioSolicitud($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'solicitudCT'){
		envioSolicitudTrinidadCLC($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'estudiosCMDLT'){
		cambiarFechaEstudiosCMDLT($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'subirInformes'){
		subirInformes($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'asociarPacientes'){
		asociarPacientes($_GET['localidad'], $logs);
	}else if($_GET['solicitud'] == 'facturacionfsyf'){
		$connect = new Connections('CORREO', 'CORREO', 'idasysi');
		facturacionfinsemanaferiado($_GET['localidad'], $connect);
	}else if($_GET['solicitud'] == 'validarFacturacion'){
		$connect = new Connections('CORREO', 'CORREO', 'idasysi');
		validarFacturacion($_GET['localidad'], $connect);
	}else if($_GET['solicitud'] == 'cambiarInstitucion'){
		cambiarInstitucion($_GET['localidad'], $logs);
	}else{
		header('HTTP/1.1 405 Method Not Allowed');
		exit;
	}

/* -------------------------------------------------------------- APROBADOS --------------------------------------------------------------- */
	
	/* Funcion que se encarga de buscar en la tabla RIS01FP los registros con estatus 02 y los envia al mediris para que lo marque como aprobado y luego se le cambie el estatus a 03 en la base de datos */
	function buscar_registros($connect, $logs){

		$consulta = $connect->select("IDASYSI.RIS01FP", "*", "ARGSTS='02'", "", "");

		if(!empty($consulta)){
			foreach ($consulta as $item) {
				if(enviar_peticion($item)){
					if(actualizar_status($connect, $item)){
						$logs->logsHandler('1000', "SE CAMBIO EN BD EL ESTATUS A LA ORDEN N°: " . $item['NUMERO_INGRESO'] . "\n", "exito", "aprobados", $localidad);
						$response = "true";
					}else{
						$logs->logsHandler('0001', "NO SE PUDO CAMBIAR EL ESTATUS A LA ORDEN N°: " . $item['NUMERO_INGRESO'] . "\n", "error", "aprobados", $localidad);
						$response = "false";
					}
				}else{
					$logs->logsHandler('0002', "EL NÚMERO DE ORDEN: " . $item['NUMERO_INGRESO'] . " YA FUE PROCESADA \n", "error", "aprobados", $localidad);
					$response = "false";
				}
			}
		}else{
			$logs->logsHandler('0003', "NO SE ENCONTRÓ NINGÚN REGISTRO", "error", "aprobados", $localidad);
			$response = "false";			
		}	
		echo $response;
	}

	/* Funcion para actualizar el status del registro una vez que fue procesado */
	function actualizar_status($connect, $item){
		$actualizacion = false;
		$numero_ingreso = $item['NUMERO_INGRESO'];
		$localidad = $item['LOCALIDAD'];

		$resultadoUpdate = $connect->update("IDASYSI.RIS01FP", "ARGSTS='03'", "AINNRO='$numero_ingreso' AND ALCCOD = '$localidad'");

		if($resultadoUpdate != "1"){
			$actualizacion = true;
		}

		return $actualizacion;
	}

	/* Funcion que envia al endpoint los datos del registro que será procesado */
	function enviar_peticion($item){
		$peticion = false;

		if($item['STATUS'] == '02' ){

			$jsonData = array('LOCALIDAD' => $item['LOCALIDAD'], 'NUMERO_INGRESO' => $item['NUMERO_INGRESO']);

			$jsonDataEncoded = json_encode($jsonData);
			
			/* Envio al endpoint */

			$url = 'https://medirisoas.idaca.com.ve/api/preadmission/approve';
			$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc29hc2lzLmlkYWNhLmNvbS52ZSIsImlhdCI6MTUxNTQ0NTY2NSwibmJmIjoxNTE1NDQ1NjY1LCJqdGkiOiI5RTdIRzh5UThiOVNDck9WIiwic3ViIjoyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.4BIvhEuMIz4jrCu49tjN2KqKcGIDew83gFo6sME5Ss0";

		 	// Definimos la url a la cual se realizara la peticion
		 	$ch = curl_init($url);

			// Indicamos que nuestra petición sera Post
			curl_setopt($ch, CURLOPT_POST, 1);

			// Para que la peticion retorne un resultado y podamos manipularlo
 			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

 			// Adjuntamos el json a nuestra petición
			curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

			// Agregamos los encabezados del contenido
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer ".$token));

          	// Utilicen estas dos lineas si su petición es tipo https y estan en servidor de desarrollo
		 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			 
			// Ejecutamos la petición
			$result = curl_exec($ch);

			// Decodeamos la respuesta
			$respuesta = json_decode($result, true);

		 	// Se cierra el recurso CURL y se liberan los recursos del sistema
		 	curl_close($ch);

		 	/* Envio al endpoint */

		 	// Evaluamos la respuesta de la url
		 	if($respuesta['success'] == "true") {
				$peticion = true;
			}
		}

		return $peticion;
	}

/* -------------------------------------------------------------- APROBADOS --------------------------------------------------------------- */

/* ------------------------------------------------------------- PREINGRESOS -------------------------------------------------------------- */
	
	/* Funcion que busca en mediris los usuarios que se encuentren en preingresos, valida que esten los datos correctos y luego verifica si ya existe el registro en la base de datos, si existe lo actualiza si no los agrega en las tablas RIS01FP y RIS02FP */
	function preingresos($connect, $localidad, $logs){
		$response = true;
		$data = buscar_registros_preingresos($localidad);

		if($data){
			foreach ($data as $item) {

				if(validarDataCabecera($item, $logs) == false){
					$response = false;
				}else{

					$item['cabecera']['NRO_DOCUM_IDENT'] = str_replace("V - ","", $item['cabecera']['NRO_DOCUM_IDENT']);

					$item['cabecera']['NRO_DOCUM_IDENT'] = str_replace("E - ","", $item['cabecera']['NRO_DOCUM_IDENT']);

					$item['cabecera']['NRO_DOCUM_IDENT'] = str_replace("-","", $item['cabecera']['NRO_DOCUM_IDENT']);

					$item['cabecera']['STATUS'] = '0' . $item['cabecera']['STATUS'];

					$solicitud = $connect->select("IDASYSI.RIS01FP", "*", "AINNRO='".$item['cabecera']['NUMERO_INGRESO']."' AND ALCCOD='".$item['cabecera']['LOCALIDAD']."'", "", "");

					if(!empty($solicitud)){
						$response = actualizarCabecera($item, $connect, $logs);
					}else{
						if(guardarCabecera($item, $connect) == false){
							$response = false;
						}else{
							foreach ($item['detalle'] as $detail) {
								if(guardarDetalles($detail, $connect, $item['cabecera']['NUMERO_INGRESO'], $item, $logs) == false){
									$response = false;
								}
							}
						}
					}
				}
			}
		}else{
			$response = false;
			$logs->logsHandler("0001", "No se encontró información", "error", "preingresos", $localidad);
		}

		echo json_encode($response);
	}

	/* Funcion para buscar en el endpoint los registros a procesar */
	function buscar_registros_preingresos($localidad){

		$conf = new Config();
		$data = $conf->configSedes($localidad);

		$url = $data['url'];
		$token = $data['token'];

	 	// Definimos la url a la cual se realizara la peticion
	 	$ch = curl_init($url."api/preadmission");

		// Indicamos que nuestra petición sera Get
		curl_setopt($ch, CURLOPT_HTTPGET, TRUE);

		// Para que la peticion retorne un resultado y podamos manipularlo
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// Agregamos los encabezados del contenido
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer ".$token));

      	// Utilicen estas dos lineas si su petición es tipo https y estan en servidor de desarrollo
	 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		 
		// Ejecutamos la petición
		$result = curl_exec($ch);

	 	// Se cierra el recurso CURL y se liberan los recursos del sistema
	 	curl_close($ch);

	 	// Respuesta del endpoint
	 	$resultado = json_decode($result, true);

	 	return $resultado;
	}

	/* Funcion para guardar en la BD los datos de la cabecera de la peticion */
	function guardarCabecera($item, $connect){

		/* FORMATO DE FECHA PARA AS400 */
		$apafen = substr($item['cabecera']['FECHA_NACIMIENT'], 0, -9);
		$item['cabecera']['FECHA_NACIMIENT'] = str_replace('-', '', $apafen);

		$registro = $connect->insert("IDASYSI.RIS01FP", "ALCCOD, AINNRO, ATPCOD, APANRO, APANOM, APANAC, APAFEN, APADIR, APATLF, APASEX, ARMCOD, ARMNOM, ATPPAS, ARGSTS", "'".$item['cabecera']['LOCALIDAD']."', '".$item['cabecera']['NUMERO_INGRESO']."', '".$item['cabecera']['TIPO_IDENTIF']."', '".$item['cabecera']['NRO_DOCUM_IDENT']."', '".$item['cabecera']['NOMBRE_PACIENTE']."', '".$item['cabecera']['NACIONALIDAD']."', '".$item['cabecera']['FECHA_NACIMIENT']."', '".$item['cabecera']['DIRECCION']."', '".$item['cabecera']['TELEFONOS']."', '".$item['cabecera']['SEXO']."', '".$item['cabecera']['CODIGO_REMITENTE']."', '".$item['cabecera']['NOMBRE_REMITENTE']."', '".$item['cabecera']['TIPO_PACIENTE']."', '".$item['cabecera']['STATUS']."'");

		if($registro == true){
			$response = true;
		}else{
			$response = false;
		}

		return $response;
	}

	/* Funcion para guardar en la BD los datos del detalle de la peticion */
	function guardarDetalles($detail, $connect, $noring, $item, $logs){
		$itemVacioDetalle = false;
		$alccod = (!empty($detail['LOCALIDAD']) ? $detail['LOCALIDAD'] : $itemVacioDetalle = true);
		$aornro = (!empty($detail['NUMERO_ORDEN']) ? $detail['NUMERO_ORDEN'] : $itemVacioDetalle = true);
		$argtip = (!empty($detail['TIPO_REGISTRO']) ? $detail['TIPO_REGISTRO'] : $itemVacioDetalle = true);
		$argcod = (!empty($detail['COD_INSUM_ESTUD']) ? $detail['COD_INSUM_ESTUD'] : $itemVacioDetalle = true);
		$argcan = (!empty($detail['CANTIDAD']) ? $detail['CANTIDAD'] : $itemVacioDetalle = true);
		$noring = (!empty($noring) ? $noring: $itemVacioDetalle = true);
		$argunm = $detail['UNIDAD_MEDIDA'];
		$argmed = $detail['CODIGO_MEDICO'];
		$argnme = $detail['NOMBRE_MEDICO'];
		$argtec = $detail['CODIGO_TECNICO'];
		$argnte = $detail['NOMBRE_TECNICO'];
		$adtnra = $detail['NUMERO_ACCION'];
		$ardsts = $detail['STATUS_DETALLE'];

		if($itemVacioDetalle == true){
			$response = false;
			$logs->logsHandler("004", "Los datos de la orden con el número ".$detail['NUMERO_ORDEN']." y el número de ingreso ".$noring." llegaron incompletos - Data: ".json_encode($detail), "error", "preingresos", $detail['LOCALIDAD']);
		}else{

			$solicitud = $connect->select("IDASYSI.RIS02FP", "*", "AINNRO = '$noring' AND ALCCOD = '$alccod' AND AORNRO = '$aornro'", "", "");

			$ardsts = '0' . $ardsts;
			
			if(!empty($solicitud)){
				
				$actualizacion = $connect->update("IDASYSI.RIS02FP", "ALCCOD = '$alccod', AINNRO = '$noring', AORNRO = '$aornro', ARGTIP = '$argtip', ARGCOD = '$argcod', ARGCAN = '$argcan', ARGUNM = '$argunm', ARGMED = '$argmed', ARGNME = '$argnme', ARGTEC = '$argtec', ARGNTE = '$argnte', ADTNRA = '$adtnra', ARDSTS = '$ardsts'", "AINNRO='$noring' AND ALCCOD='$alccod' AND AORNRO = '$aornro'");

				if($actualizacion == null){
					$response = true;
				}
			}else{

				$response = $connect->insert("IDASYSI.RIS02FP", "ALCCOD, AINNRO, AORNRO, ARGTIP, ARGCOD, ARGCAN, ARGUNM, ARGMED, ARGNME, ARGTEC, ARGNTE, ADTNRA, ARDSTS", "'$alccod', '$noring', '$aornro', '$argtip', '$argcod', '$argcan', '$argunm', '$argmed', '$argnme', '$argtec', '$argnte', '$adtnra', '$ardsts'");
			}

			if($response){
				$logs->logsHandler("0005", "Los datos de la orden con el número ".$detail['NUMERO_ORDEN']." y el número de ingreso ".$noring." se guardaron bien - Data: " .json_encode($item), "exito", "preingresos", $detail['LOCALIDAD']);
			}

		}

		return $response;
	}

	/* Funcion para actualizar en la BD los datos de la cabecera de la peticion */
	function actualizarCabecera($item, $connect, $logs){

		/* FORMATO DE FECHA PARA AS400 */
		$apafen = substr($item['cabecera']['FECHA_NACIMIENT'], 0, -9);
		$item['cabecera']['FECHA_NACIMIENT'] = str_replace('-', '', $apafen);

		$response = $connect->update("IDASYSI.RIS01FP", "ALCCOD = '".$item['cabecera']['LOCALIDAD']."', AINNRO = '".$item['cabecera']['NUMERO_INGRESO']."', ATPCOD = '".$item['cabecera']['TIPO_IDENTIF']."', APANRO = '".$item['cabecera']['NRO_DOCUM_IDENT']."', APANOM = '".$item['cabecera']['NOMBRE_PACIENTE']."', APANAC = '".$item['cabecera']['NACIONALIDAD']."', APAFEN = '".$item['cabecera']['FECHA_NACIMIENT']."', APADIR = '".$item['cabecera']['DIRECCION']."', APATLF = '".$item['cabecera']['TELEFONOS']."', APASEX = '".$item['cabecera']['SEXO']."', ARMCOD = '".$item['cabecera']['CODIGO_REMITENTE']."', ARMNOM = '".$item['cabecera']['NOMBRE_REMITENTE']."', ATPPAS = '".$item['cabecera']['TIPO_PACIENTE']."', ARGSTS = '".$item['cabecera']['STATUS']."'", "AINNRO='".$item['cabecera']['NUMERO_INGRESO']."' AND ALCCOD='".$item['cabecera']['LOCALIDAD']."'");

		if($response == null){
			foreach ($item['detalle'] as $detail) {
				$eliminar = $connect->delete("IDASYSI.RIS02FP", "ALCCOD = '".$item['cabecera']['LOCALIDAD']."' AND AINNRO = '".$item['cabecera']['NUMERO_INGRESO']."' AND AORNRO = '".$detail['NUMERO_ORDEN']."'");
				$response = guardarDetalles($detail, $connect, $item['cabecera']['NUMERO_INGRESO'], $item, $logs);
			}
		}else{
			$response = false;
		}

		return $response;
	}

	/* Funcion para validar que los datos de la peticion no esten vacios */
	function validarDataCabecera($item, $logs){
		$itemVacioCabecera = false;
		$item['cabecera']['LOCALIDAD'] = (!empty($item['cabecera']['LOCALIDAD']) ? $item['cabecera']['LOCALIDAD'] : $itemVacioCabecera = true);
		$item['cabecera']['NUMERO_INGRESO'] = (!empty($item['cabecera']['NUMERO_INGRESO']) ? $item['cabecera']['NUMERO_INGRESO'] : $itemVacioCabecera = true);
		$item['cabecera']['TIPO_IDENTIF'] = (!empty($item['cabecera']['TIPO_IDENTIF']) ? $item['cabecera']['TIPO_IDENTIF'] : $itemVacioCabecera = true);
		$item['cabecera']['NRO_DOCUM_IDENT'] = (!empty($item['cabecera']['NRO_DOCUM_IDENT']) ? $item['cabecera']['NRO_DOCUM_IDENT'] : $itemVacioCabecera = true);
		$item['cabecera']['NOMBRE_PACIENTE'] = (!empty($item['cabecera']['NOMBRE_PACIENTE']) ? $item['cabecera']['NOMBRE_PACIENTE'] : $itemVacioCabecera = true);
		$item['cabecera']['NACIONALIDAD'] = (!empty($item['cabecera']['NACIONALIDAD']) ? $item['cabecera']['NACIONALIDAD'] : $itemVacioCabecera = true);
		$item['cabecera']['FECHA_NACIMIENT'] = (!empty($item['cabecera']['FECHA_NACIMIENT']) ? $item['cabecera']['FECHA_NACIMIENT'] : $itemVacioCabecera = true);
		$item['cabecera']['DIRECCION'] = (!empty($item['cabecera']['DIRECCION']) ? $item['cabecera']['DIRECCION'] : $itemVacioCabecera = true);
		// $item['cabecera']['TELEFONOS'] = (!empty($item['cabecera']['TELEFONOS']) ? $item['cabecera']['TELEFONOS'] : $itemVacioCabecera = true);
		$item['cabecera']['SEXO'] = (!empty($item['cabecera']['SEXO']) ? $item['cabecera']['SEXO'] : $itemVacioCabecera = true);
		$item['cabecera']['TIPO_PACIENTE'] = (!empty($item['cabecera']['TIPO_PACIENTE']) ? $item['cabecera']['TIPO_PACIENTE'] : $itemVacioCabecera = true);

		if($itemVacioCabecera == true){
			$response = false;
			$logs->logsHandler("003", "Los datos de la solicitud con el número de ingreso ".$item['cabecera']['NUMERO_INGRESO']." llegaron incompletos - Data: ".json_encode($item), "error", "preingresos", $item['cabecera']['LOCALIDAD']);
		}else{
			$response = $item;
		}

		return $response;
	}

/* ------------------------------------------------------------- PREINGRESOS -------------------------------------------------------------- */

/* --------------------------------------------------------------- REPORTE ---------------------------------------------------------------- */

	/* Funcion que se encarga de enviar a la plataforma de viewmed los informes de los estuidos de las sedes oasis, floresta y sanatrix. Esta funcion es llamada desde el mediris */
	function reporte($localidad, $logs){

		$user = "mediris";

		$datos = explode("-", $localidad);
		$localidad = $datos[0];
		$orden = $datos[1];
		$tipo = $datos[2];

		$conf = new Config();
		$data = $conf->configSedes($localidad);

		$host = $data['host'];
		$pwd = $data['pwd'];
		$url = $data['url'];
		$user_agent = $data['user_agent'];
		$id_institucion = $data['id_institucion'];

		$mysqli = new mysqli($host,  $user, $pwd);
		$mysqli->set_charset("utf8");
		if ($mysqli->connect_errno) {
		    echo $mysqli->connect_error;
		    exit();
		}else{

			$query = "SELECT T1.service_request_id, T1.procedure_id, T1.text, T1.radiologist_user_name, T1.radiologist_user_id, T1.approve_user_name, T1.approve_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T2.height, T3.administrative_ID as sexo, T4.birth_date, T4.email, T4.cellphone_number, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T9.email as email_referido, T9.administrative_ID, T10.report_header, T10.report_footer FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN `mediris`.`sexes` T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN `mediris`.`patients` T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN `mediris`.`users` T7 ON(T7.id = T1.approve_user_id) LEFT JOIN `mediris`.`prefixes` T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN `mediris`.`institutions` T10 ON(T10.administrative_id = '".$localidad."') WHERE T1.id = '".$orden."'";

			$resultado = $mysqli->query($query);

			$datos = $resultado->fetch_array(MYSQLI_ASSOC);

			if($tipo == 'A'){

				$query = "SELECT text FROM apimeditron.addendums where requested_procedure_id = '".$orden."' ORDER BY id DESC LIMIT 1";

				$resultado = $mysqli->query($query);
				$txt = $resultado->fetch_array(MYSQLI_ASSOC);
				$datos['text'] = $txt['text'];

			}

			$resultado->close();

			if(stripos($datos['administrative_ID'], 'j') !== false || stripos($datos['administrative_ID'], 'g') !== false){
				$referred_institution = $datos['nombre_referido'] . " " . $datos['apellido_referido'];
				$email_ref_institution = $datos['email_referido'] == 'noespecificado@noespecificado.com' ? '' : validateEmail($datos['email_referido']);
				$referred_physician = "";
				$email_ref_physician = "";
			}else{
				$referred_institution = "";
				$email_ref_institution = "";
				$referred_physician = $datos['nombre_referido'] . " " . $datos['apellido_referido'];
				$email_ref_physician = validateEmail($datos['email_referido']);
			}

			$datos['signature'] = str_replace("users","firmas",$datos['signature']);


			$mimeHeader = obtenerMime($datos['report_header'], $url);
			$headerB64 = obtenerB64($datos['report_header'], $url);

			// mime de las imagenes (header, footer y firma del medico)
			$mimeFooter = obtenerMime($datos['report_footer'], $url);
			$mimeSignature = obtenerMime($datos['signature'], $url);

			// base64 de las imagenes (header, footer y firma del medico
			$footerB64 = obtenerB64($datos['report_footer'], $url);
			$signatureB64 = obtenerB64($datos['signature'], $url);

			$phone = '';
			if($datos['cellphone_number']){
				$phone = substr($datos['cellphone_number'],1);
				$phone = '+58'.$phone;
			}
			
			$data = array("accession_number" => $orden, "institution" => $id_institucion, "patient_identification" => $datos['patient_identification_id'], "patient_phone" => $phone, "patient_name" => $datos['patient_first_name'] . " " . $datos['patient_last_name'], "patient_gender" => $datos['sexo'], "patient_birth" => $datos['birth_date'] . ".000", "patient_email" => validateEmail($datos['email']), "referred_institution" => $referred_institution, "email_ref_institution" => $email_ref_institution, "referred_physician" => $referred_physician, "email_ref_physician" => $email_ref_physician, "date_report" => $datos['culmination_date'] . ".000", "attached" => [], "study_date" => $datos['issue_date'] . ".000", "study_type" => $datos['patient_type'], "study_name" => $datos['procedimiento'], "physician_name" => $datos['prexif'] . " " .$datos['approve_user_name'], "physician_specialty" => $datos['position'], "tag1" => array("Estatura" => $datos['height'] . " m"), "tag2" => null, "tag3" => null, "report" => array("header" => array("mime" => $mimeHeader, "content" => $headerB64), "body" => array("mime" => 'text/html', "content" => $datos['text']), "footer" => array("mime" => $mimeFooter, "content" => $footerB64), "signature" => array("mime" => $mimeSignature, "content" => $signatureB64)));

			$response = enviarInforme($data, $user_agent);
			
			$logs->logsHandler('0007', "Respuesta Viewmed orden #: " . $orden . " - " . $response ."\r\n\n----------------------------------------------------------------------------------------------------------------------------------- \r\n\n", "exito", "reportes", $localidad);
			
			echo json_encode($response);
		}
		
		$mysqli->close();
	}


	function obtenerMime($file, $url){
		
		if($file){
		
			$arrContextOptions=array(
			    "ssl"=>array(
			        "verify_peer"=>false,
			        "verify_peer_name"=>false,
			    ),
			);

			$img = file_get_contents($url. "/storage/" . $file, false, stream_context_create($arrContextOptions));

			$fileArray = pathinfo($file); 
			
			$nameImg = $fileArray['filename'];

			file_put_contents($nameImg, $img);

			$mime = mime_content_type($nameImg);

			unlink($nameImg);

			return $mime;
		}else{
			return "";
		}
	}

	
	function obtenerB64($file, $url){
		if($file){

			$arrContextOptions=array(
			    "ssl"=>array(
			        "verify_peer"=>false,
			        "verify_peer_name"=>false,
			    ),
			);

			return base64_encode(file_get_contents($url. "/storage/" . $file, false, stream_context_create($arrContextOptions)));
		}else{
			return "";
		}
	}


	function validateEmail($email){
		if(filter_var($email, FILTER_VALIDATE_EMAIL) == false){
			$email = "sinespecificar@email.com";
		}
			
		return $email;
	}


	function enviarInforme($data, $user_agent){

		$url = "https://api.viewmedonline.com:8443/medical/report/upload";
		//$url = "https://dev.viewmedonline.com:8443/medical/report/upload";

		/* Se encodea el JSON para enviarlo */
		$jsonDataEncoded = json_encode($data);

	 	// Definimos la url a la cual se realizara la peticion
	 	$ch = curl_init($url);

		// Indicamos que nuestra petición sera Post
		curl_setopt($ch, CURLOPT_POST, 1);

		// Para que la peticion retorne un resultado y podamos manipularlo
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// Adjuntamos el json a nuestra petición
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

		// Agregamos los encabezados del contenido
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "User-Agent: " .$user_agent));
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "User-Agent: " .$user_agent, "Authorization: Bearer ".$token));

		curl_setopt($ch, CURLOPT_HEADER, 0);

      	// Utilicen estas dos lineas si su petición es tipo https y estan en servidor de desarrollo
	 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		 
		// Ejecutamos la petición y almacenamos la respuesta en la variable $result
		$result = curl_exec($ch);

	 	// Se cierra el recurso CURL y se liberan los recursos del sistema
	 	curl_close($ch);

	 	return $result;
	}

/* --------------------------------------------------------------- REPORTE ---------------------------------------------------------------- */

/* ---------------------------------------------------------------- CORREO ---------------------------------------------------------------- */

	/* Funcion que se utiliza para cambiar en viewmed el correo del paciente y ellos reenvian el informe al correo nuevo. Esta funcion es llamada desde el sistema de informes */
	function correo($localidad, $logs){

		$user = "mediris";

		$datos = explode("-", $localidad);
		$localidad = $datos[0];
		$orden = $datos[1];

		$conf = new Config();
		$data = $conf->configSedes($localidad);

		$host = $data['host'];
		$pwd = $data['pwd'];
		$url = $data['url'];
		$user_agent = $data['user_agent'];
		$id_institucion = $data['id_institucion'];
		
		$mysqli = new mysqli($host,  $user, $pwd);
		$mysqli->set_charset("utf8");
		if ($mysqli->connect_errno) {
		    echo $mysqli->connect_error;
		    exit();
		}else{
			
			$query = "SELECT T1.service_request_id, T2.id, T2.patient_id, T2.patient_identification_id, T3.email FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN `mediris`.`patients` T3 ON(T3.id = T2.patient_id) WHERE T1.id = '".$orden."'";

			$resultado = $mysqli->query($query);

			$datos = $resultado->fetch_array(MYSQLI_ASSOC);
			
			if($datos['email'] == 'sincorreo@idaca.com.ve'){
				$response = array("status" => '500', "mensaje" => 'Correo inválido', "data" => '');
			}else{
				if(validateEmail($datos['email']) != 'sinespecificar@email.com'){

					$data = array("accession_number" => $orden, "institution" => $id_institucion, "patient_identification" => $datos['patient_identification_id'], "new_patient_email" => $datos['email']);
					
					$response = actualizarCorreo($data, $user_agent);
					
				}else{
					$response = array("status" => '500', "mensaje" => 'Correo inválido', "data" => '');
				}
			}
			
			$logs->logsHandler('0008', "Respuesta Viewmed orden #: " . $orden . " - " . $response ."\r\n\n----------------------------------------------------------------------------------------------------------------------------------- \r\n\n", "exito", "correos", $localidad);
			
			echo json_encode($response);
		}
	}
		
		
	function actualizarCorreo($data, $user_agent){

		$url = "https://api.viewmedonline.com:8443/medical/report/modify";
		//$url = "https://dev.viewmedonline.com:8443/medical/report/modify";

		/* Se encodea el JSON para enviarlo */
		$jsonDataEncoded = json_encode($data);
		
		// Definimos la url a la cual se realizara la peticion
		$ch = curl_init($url);
		
		// Indicamos que nuestra petición sera PUT	
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		
		// Para que la peticion retorne un resultado y podamos manipularlo
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		// Adjuntamos el json a nuestra petición
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
		
		// Agregamos los encabezados del contenido
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "User-Agent: " .$user_agent));
		
		curl_setopt($ch, CURLOPT_HEADER, 0);
		
		// Utilicen estas dos lineas si su petición es tipo https y estan en servidor de desarrollo
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	
		// Ejecutamos la petición y almacenamos la respuesta en la variable $result
		$result = curl_exec($ch);
		
		// Se cierra el recurso CURL y se liberan los recursos del sistema
		curl_close($ch);

		return $result;
	}
		
/* ---------------------------------------------------------------- CORREO ---------------------------------------------------------------- */

/* --------------------------------------------------------------- REPORTE TRINIDAD ---------------------------------------------------------------- */
	
	/* Funcion que se encarga de subir a la plataforma del viewmed los informes de los estudios de la trinidad. Esto lo ejecuta el servicio de windows */
	/*function reporteTrinidadCLC($localidad, $logs){

		$user = 'root';
		$pwd = "017F22bed";
		
		if($localidad == '04'){
			$host = "192.168.9.2:3306";
			$user_agent = "Las_Ciencias";
			$id_institucion = "5fc044c3a2be3a1fa623faf2";
		}else{
			$host = "192.168.11.30:3306";
			$user_agent = "IDACA - CMDT";
			$id_institucion = "5f6cd01f9e80a3612d70ead4";
		}

		$mysqli = new mysqli($host,  $user, $pwd);
		$mysqli->set_charset("utf8");

		if($mysqli->connect_errno){
		    echo $mysqli->connect_error;
		    exit();
		}else{
			
			$fechaIni = date('Y') . "-" . date('m') . "-" . date('d') . " 00:00:00";
			$fechaFin = date('Y') . "-" . date('m') . "-" . date('d') . " 23:59:59";

			$query = "SELECT T1.id, T1.texto, T1.fecha_dictado, T1.fecha_transcripcion, T1.fecha_Aprobacion, T1.id_dictador, T1.id_aprobador, T1.id_transcriptora, T2.accession_number, T2.fecha_inicio, T2.fechaRealizacion, T2.id_procedimiento, T2.id_addemdum, T2.id_estatus, T2.id_solicitud, T2.id_informe_trascrito, T2.costoTranscriptora, T3.fecha_solicitud, T3.id_paciente, T3.id_tipo_paciente, T4.nombres, T4.apellidos, T4.cedula, T4.sexo, T4.fecha_nacimiento, T4.peso, T4.altura, T4.email as correo, T4.telefonoFijo, T4.telefonoMovil as cellphone_number, T4.tipo_doc, T5.descripcion as procedimiento, T5.id_modalidad, T6.descripcion as modalidad, T6.codigo_dicom, T7.descripcion as tipoPaciente, T8.nombre as nombreDictador, T8.apellido  as apellidoDictador, T8.email, T8.firma, T8.titulo, T8.informacion_firma FROM `veris2`.`informe` T1 LEFT JOIN `veris2`.`orden` T2 ON(T2.id_informe_trascrito = T1.id) LEFT JOIN `veris2`.`solicitud` T3 ON(T3.id = T2.id_solicitud) LEFT JOIN `veris2`.`paciente` T4 ON(T4.id = T3.id_paciente) LEFT JOIN `veris2`.`procedimiento` T5 ON(T5.id = T2.id_procedimiento) LEFT JOIN `veris2`.`modalidad` T6 ON(T6.id = T5.id_modalidad) LEFT JOIN `veris2`.`tipo_paciente` T7 ON(T7.id = T3.id_tipo_paciente) LEFT JOIN `veris2`.`usuario` T8 ON(T8.id = T1.id_dictador) WHERE T1.fecha_Aprobacion BETWEEN '".$fechaIni."' AND '".$fechaFin."'";

			$resultado = $mysqli->query($query);

			while ($fila = $resultado->fetch_array(MYSQLI_ASSOC)) {

				if($fila['costoTranscriptora'] == 0){

					if(!empty($fila['id_addemdum'])){
					
						$query1 = "SELECT * FROM  `veris2`.`addendum` WHERE id = '".$fila['id_addemdum']."'";
						$resultado1 = $mysqli->query($query1);

						$datos = $resultado1->fetch_array(MYSQLI_ASSOC);

						$fila['texto'] = $datos['texto'];
					}

					$fila['texto'] = limpiarTexto($fila['texto']);

					if($localidad == '04'){
						$mimeHeader = obtenerMimeCMDLT('logo_lasciencias.png', './img');
						$headerB64 = obtenerB64CMDLT('logo_lasciencias.png', './img');
					}else{
						$mimeHeader = obtenerMimeCMDLT('logo-cmdlt.JPG', './img');
						$headerB64 = obtenerB64CMDLT('logo-cmdlt.JPG', './img');
					}

					$mimeFooter = obtenerMimeCMDLT('footercmdlt.png', './img');
					$footerB64 = obtenerB64CMDLT('footercmdlt.png', './img');

					if(!empty($fila['firma'])){
						$mimeSignature = 'image/jpeg';
						$signatureB64 = base64_encode($fila['firma']);
					}else{
						$mimeSignature = '';
						$signatureB64 = '';
					}

					$fila['nombreDictador'] = str_replace("Dr.", "", $fila['nombreDictador']);

					$phone = '';

					if($fila['cellphone_number']){
						$phone = substr($fila['cellphone_number'],1);
						$phone = '+58'.$phone;
					}

					$data = array("accession_number" => $fila['accession_number'], 'institution' => $id_institucion, "patient_identification" => $fila['cedula'], "patient_phone" => $phone, "patient_name" => $fila['nombres'] . " " . $fila['apellidos'], "patient_gender" => $fila['sexo'], "patient_birth" => $fila['fecha_nacimiento'] . " 00:00:00.000", "patient_email" => validateEmail($fila['correo']), "referred_institution" => '', "email_ref_institution" => '', "referred_physician" => '', "email_ref_physician" => '', "date_report" => $fila['fecha_Aprobacion'] . ".000", "attached" => [], "study_date" => $fila['fechaRealizacion'] . ".000", "study_type" => $fila['tipoPaciente'], "study_name" => $fila['procedimiento'], "physician_name" => $fila['titulo'] . " " .$fila['nombreDictador'] . " " .$fila['apellidoDictador'], "physician_specialty" => $fila['informacion_firma'], "tag1" => array("Estatura" => $fila['altura'] . " m"), "tag2" => null, "tag3" => null, "report" => array("header" => array("mime" => $mimeHeader, "content" => $headerB64), "body" => array("mime" => 'text/html', "content" => $fila['texto']), "footer" => array("mime" => $mimeFooter, "content" => $footerB64), "signature" => array("mime" => $mimeSignature, "content" => $signatureB64)));

					//echo json_encode($data) . "\n\n";

					$response = enviarInforme($data, $user_agent);
					$logs->logsHandler('0007', "Respuesta Viewmed orden #: " . $fila['accession_number'] . " - " . $response ."\r\n\n----------------------------------------------------------------------------------------------------------------------------------- \r\n\n", "exito", "reportes", $localidad);

					$respuesta = json_decode($response, true);

					if(!empty(json_encode($respuesta['documents'][0]['_id']))){
						$upd = "UPDATE `veris2`.`orden` SET `costoTranscriptora` = '1' WHERE id = '".$fila['accession_number']."'";
						$resultadoupd = $mysqli->query($upd);
					}
					
					echo json_encode($response);
				}
			}
	
			$resultado->close();
		}

		
		$mysqli->close();

	}


	function obtenerMimeCMDLT($file, $url){
		
		$img = file_get_contents($url. "/" . $file);

		$fileArray = pathinfo($file);
		
		$nameImg = $fileArray['filename'];

		file_put_contents($nameImg, $img);

		$mime = mime_content_type($nameImg);

		unlink($nameImg);

		return $mime;
	}

	
	function obtenerB64CMDLT($file, $url){
		return base64_encode(file_get_contents($url. "/" . $file));
	}


	function limpiarTexto($texto){
		
		$texto = str_replace("<SPAN lang=ES-MODERN>", "", $texto);
		$texto = str_replace("<SPAN lang=ES-VE>", "", $texto);
		$texto = str_replace("</SPAN>", "", $texto);
		$texto = str_replace("\r\n", "", $texto);
		$texto = str_replace("<P>", "", $texto);
		$texto = str_replace("<P align=justify>", "&lt;p&gt;", $texto);
		$texto = str_replace("</P>", "&lt;/p&gt;", $texto);
		$texto = str_replace("<STRONG>", "&lt;strong&gt;", $texto);
		$texto = str_replace("</STRONG>", "&lt;/strong&gt;", $texto);
		$texto = str_replace("<EM>", "&lt;em&gt;", $texto);
		$texto = str_replace("</EM>", "&lt;/em&gt;", $texto);
		$texto = str_replace("<U>", '&lt;span style="text-decoration:underline;"&gt;', $texto);
		$texto = str_replace("</U>", "&lt;/span&gt;", $texto);
		$texto = str_replace("<BR>", "&lt;p&gt;&amp;nbsp&lt;/p&gt;", $texto);
		$texto = str_replace("<FONT size=2>", "", $texto);
		$texto = str_replace("</FONT>", "", $texto);
		$texto = str_replace("<FONT size=2 face=Arial>", "", $texto);

		return $texto;
	}*/

/* --------------------------------------------------------------- REPORTE TRINIDAD ---------------------------------------------------------------- */

/* --------------------------------------------------------------- FACTURACION ---------------------------------------------------------------- */
	
	/* Funcion que se encarga de buscar los estudios que se encuentran subidos a la plataforma de viewmed en un rango de fecha y compara con los estudios que se encuentran en el mediris o ris en ese mismo rango de fechas para determinar cuales estan arriba en viewmed y cuales no para el pago. Esto es llamado desde el centralizador o desde el sistema de informes */
	function facturacion($localidad, $connect, $logs){

		$datos = explode("-", $localidad);

		$localidad = $datos[0];
		$start_date = str_replace("_", "-", $datos[1]);
        $end_date = str_replace("_", "-", $datos[2]);
        $user = 'mediris';

        $conf = new Config();
		$data = $conf->configSedes($localidad);

		$host = $data['host'];
		$pwd = $data['pwd'];
		$url = $data['url'];
		$user_agent = $data['user_agent'];
		$id_institucion = $data['id_institucion'];

		$data = array("institution" => $id_institucion, 'from' => $start_date, "to" => $end_date);

		$estudios = solicitarEstudios($data);

		if(count($estudios) > 0){

			$mysqli = new mysqli($host,  $user, $pwd);
			$mysqli->set_charset("utf8");

			if($mysqli->connect_errno){
			    echo $mysqli->connect_error;
			    exit();
			}else{

				$info = [];

				foreach ($estudios as $key => $value){

					if($value['accession_number'] != null){

						$query = "SELECT T1.id, T1.service_request_id, T1.procedure_id, T1.requested_procedure_status_id, T1.culmination_date, T2.description, T2.modality_id, T2.administrative_ID, T3.description AS status, T4.name AS modalidad, T5.patient_identification_id FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`procedures` T2 ON(T2.id = T1.procedure_id) LEFT JOIN `apimeditron`.`requested_procedure_statuses` T3 ON(T3.id = T1.requested_procedure_status_id)LEFT JOIN `apimeditron`.`modalities` T4 ON(T4.id = T2.modality_id) LEFT JOIN `apimeditron`.`service_requests` T5 ON(T5.id = T1.service_request_id) WHERE T1.id = '".$value['accession_number']."'";

						$resultado = $mysqli->query($query);

						$datos = $resultado->fetch_array(MYSQLI_ASSOC);

						if(count($datos) > 0){
							$datos['status'] = convertStatus($datos['status']);

							if($datos['culmination_date'] != '0000-00-00 00:00:00'){
								$datos['culmination_date'] = date("d/m/Y", strtotime($datos['culmination_date']));
							}

							$imgPacs = validarImagenPACS($localidad."-".$value['accession_number'], $logs, 1);

							if($imgPacs['busqueda'] > 0){
								$datos['pacs'] = true;
							}else{
								$datos['pacs'] = false;
							}

							$datos['nimagenesviewmed'] = $value['number_imagings'];
							$datos['nimagenespacs'] = $imgPacs['busqueda'];

							$datos['informe'] = $value['report'] == true ? 'Si' : 'No';

							$accession_number = str_pad($value['accession_number'], 12, 0, STR_PAD_LEFT);

							//$info_factura = $connect->select("IDASYSF.ID76FL86", "ADDNRF, ADDSTS, ARGNRO, ARGTTE, ARGTTM, ARGTTT, ARGSER, ADDFEF", "ACCCIA = '14' AND ADDLOF = '".$localidad."' AND ADRCOD IN('OR', 'O2', 'O3') AND ARQVAL = '".$accession_number."' AND ARGTIP = 'S' AND ARGSER NOT LIKE '%-O%' AND ADDSTS <> 10", "", "");

							$info_factura = $connect->select("IDASYSF.ID76FL86", "ADDNRF, ADDSTS, ARGNRO, ARGTTE, ARGTTM, ARGTTT, ARGSER, ADDFEF, ARGCOD", "ACCCIA = '14' AND ADDLOF = '".$localidad."' AND ADRCOD IN('OR', 'O2', 'O3') AND ARQVAL = '".$accession_number."' AND ARGTIP = 'S' AND ARGSER NOT LIKE '%-O%' AND ADDSTS <> 10", "", "");

							if(count($info_factura) > 0){

								$datos['total_estudio'] = 0;
								$datos['total_medico'] = 0;
								$datos['total_tecnico'] = 0;
								$datos['total'] = 0;

								$ordenes = [];

								foreach($info_factura as $a){

									if($a['ADDNRF'] != 0){

										$datos['numero_factura'] = $a['ADDNRF'];
										$datos['estatus_factura'] = $a['ADDSTS'] != '09' || $a['ADDSTS'] != '10' ? 'Emitida' : 'Anulada';
										$datos['fecha_factura'] = $a['ADDFEF'];

										$dat = $connect->select("IDASYSF.ID76FP", "ARGTIP, ARGTSV, ARGSER, ARGREM, ARGRET, ARGTTE, ARGTTM, ARGTTT", "ACCCIA = '14' AND ADDLOF = '".$localidad."' AND ARGNRO = '".$a['ARGNRO']."' AND ADDNRF = '".$a['ADDNRF']."'", "", "");

										if($a['ARGSER'] == 'COMB'){

											$ordFact = $connect->select("IDASYSF.ID76FL86", "ARQVAL", "ACCCIA = '14' AND ADDLOF = '".$localidad."' AND ADDNRF = '".$a['ADDNRF']."' AND ARGSER = 'COMB'", "", "");

											foreach($ordFact as $b){
												if(substr_count($b['ARQVAL'], 0) < 11 ){
													array_push($ordenes, $b['ARQVAL']);
												}
											}

											$cont = 0;
											foreach($ordenes as $c){
												if($accession_number == $c && $cont == 0){
													$datos['total_estudio'] += round($a['ARGTTE'], 2);
													$datos['total_medico'] += round($a['ARGTTM'], 2) - round($dat[0]['ARGREM'], 2);
													$datos['total_tecnico'] += round($a['ARGTTT'], 2) - round($dat[0]['ARGRET'], 2);
													$datos['total'] = $datos['total_estudio'] + $datos['total_medico'] + $datos['total_tecnico'];
												}
												$cont++;
											}
											$datos['contador'] = count($ordenes);

											// Si el combo es de dos estudios se aplica el 1.62% si es de 3 estudios se aplica 1.57%
											if(count($ordenes) == '2'){
												$porcentaje = '1.62';
											}else if(count($ordenes) == '3'){
												$porcentaje = '1.57';
											}

										}else{

											$datos['total_estudio'] += round($a['ARGTTE'], 2);
											$datos['total_medico'] += round($a['ARGTTM'], 2) - round($dat[0]['ARGREM'], 2);
											$datos['total_tecnico'] += round($a['ARGTTT'], 2) - round($dat[0]['ARGRET'], 2);
											$datos['total'] = $datos['total_estudio'] + $datos['total_medico'] + $datos['total_tecnico'];

											//
											// RM = MR
											// TM = CT
											// US = US
											// MG = MG
											// RX = CR/DX
											// DS = OT
											//
											if($datos['modalidad'] == 'MR' || $datos['modalidad'] == 'CT'){
												$porcentaje =  '1.25';
											}else if($datos['modalidad'] == 'OT' || $datos['modalidad'] == 'US'){
												$porcentaje =  '1.50';
											}else if($datos['modalidad'] == 'MG'){
												$porcentaje =  '1.75';
											}else if($datos['modalidad'] == 'CR' || $datos['modalidad'] == 'DX'){
												$porcentaje =  '2.00';
											}
										}


										$proyecciones = $connect->select("IDASYSF.ID76FP", "ARGCAN", "ACCCIA = '14' AND ADDLOF = '".$localidad."' AND ADDNRF = '".$a['ADDNRF']."' AND ARGCOD = '06001' AND ARGCOE = '".$a['ARGCOD']."' ", "", "");

										if(count($proyecciones) > 0){
											$datos['proyecciones_adicionales'] = $proyecciones[0]['ARGCAN'];
										}else{
											$datos['proyecciones_adicionales'] = 0;
										}


										$datos['porcentaje_viewmed'] = round($datos['total']*$porcentaje/100, 2);
									}
								}

							}else{
								$datos['numero_factura'] = 0;
								$datos['estatus_factura'] = 0;
								$datos['total_estudio'] = 0;
								$datos['total_medico'] = 0;
								$datos['total_tecnico'] = 0;
								$datos['total'] = 0;
								$datos['porcentaje_viewmed'] = 0;
								$datos['fecha_factura'] = 0;
								$datos['proyecciones_adicionales'] = 0;
							}

							array_push($info, $datos);
						}

						$resultado->close();
					}
				}
				
				echo json_encode($info);
			}
			
		}else{
			echo "No hay reportes con los criterios seleccionados";
		}
	}


	function solicitarEstudios($data){

		$url = "https://api.viewmedonline.com:8443/studies/agg/uploaded";

		// Se encodea el JSON para enviarlo 
		$jsonDataEncoded = json_encode($data);

	 	// Definimos la url a la cual se realizara la peticion
	 	$ch = curl_init($url);

		// Indicamos que nuestra petición sera Post
		curl_setopt($ch, CURLOPT_POST, 1);

		// Indicamos que nuestra petición sera Get pero podremos enviar un body
		//curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );

		// Para que la peticion retorne un resultado y podamos manipularlo
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// Adjuntamos el json a nuestra petición
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

		// Agregamos los encabezados del contenido
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

		curl_setopt($ch, CURLOPT_HEADER, 0);

      	// Utilicen estas dos lineas si su petición es tipo https y estan en servidor de desarrollo
	 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		// Ejecutamos la petición y almacenamos la respuesta en la variable $result
		$result = curl_exec($ch);

	 	// Se cierra el recurso CURL y se liberan los recursos del sistema
	 	curl_close($ch);

	 	$respuesta = json_decode($result, true);

	 	return $respuesta['documents'];

	}


	function convertStatus($status){

		$estatus = array('to-admit' => 'Por admitir',  'to-do' => 'Por realizar', 'to-dictate' => 'Por dictar', 'to-transcribe' => 'Por transcribir', 'to-approve' => 'Por aprobar', 'finished' => 'Finalizado', 'suspended' => 'Suspendido');

		return $estatus[$status];
	}

/* --------------------------------------------------------------- FACTURACION ---------------------------------------------------------------- */

/* --------------------------------------------------------------- CAMBIO DE CEDULAS ---------------------------------------------------------------- */

	/* Funcion que se encarga de realizar el cambio de cedula del pacientes en la plataforma de viewmed. Cambia la cedula en el estudio y en el informe. Esto es llamado desde el sistema de informes */
	function cambiarCedula($localidad, $logs){

		$datos = explode("-", $localidad);

		$conf = new Config();
		$data = $conf->configSedes($datos[0]);

		$user_agent = $data['user_agent'];
		$id_institucion = $data['id_institucion'];

        $data = array("accession_number" => $datos[1], 'institution' => $id_institucion, "patient_identification" => $datos[3], "new_patient_identification" => $datos[2]);

        $respuesta = cambiarCedulaEstudio($data);

        if($respuesta[0]['modifiedCount'] == 1){
        	
        	$response = cambiarCedulaInforme($data);

        	if($response[0]['modifiedCount'] == 1){
        		$res = array("status" => '200', "mensaje" => 'Se cambio la cédula correctamente', "data" => $response[0]);
        	}else{
        		$res = array("status" => '201', "mensaje" => 'Se cambio la cédula en el estudio pero no en el informe', "data" => $response[0]);
        	}
        	
        }else{
        	$res = array("status" => '500', "mensaje" => 'No se pudo cambiar la cédula en el informe ni en el estudio', "data" => $respuesta[0]);
        }

        echo json_encode($res);
	}


	function cambiarCedulaEstudio($data){

		$url = "https://api.viewmedonline.com:8443/imagings/modify";
		//$url = "https://dev.viewmedonline.com:8443/imagings/modify";

		// Se encodea el JSON para enviarlo 
		$jsonDataEncoded = json_encode($data);

	 	// Definimos la url a la cual se realizara la peticion
	 	$ch = curl_init($url);

		// Indicamos que nuestra petición sera Get pero podremos enviar un body
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );

		// Para que la peticion retorne un resultado y podamos manipularlo
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// Adjuntamos el json a nuestra petición
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

		// Agregamos los encabezados del contenido
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

		curl_setopt($ch, CURLOPT_HEADER, 0);

      	// Utilicen estas dos lineas si su petición es tipo https y estan en servidor de desarrollo
	 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		 
		// Ejecutamos la petición y almacenamos la respuesta en la variable $result
		$result = curl_exec($ch);

	 	// Se cierra el recurso CURL y se liberan los recursos del sistema
	 	curl_close($ch);

	 	$respuesta = json_decode($result, true);

	 	return $respuesta['documents'];
	}


	function cambiarCedulaInforme($data){

		$url = "https://api.viewmedonline.com:8443/medical/report/modify";
		//$url = "https://dev.viewmedonline.com:8443/medical/report/modify";

		// Se encodea el JSON para enviarlo 
		$jsonDataEncoded = json_encode($data);

	 	// Definimos la url a la cual se realizara la peticion
	 	$ch = curl_init($url);

		// Indicamos que nuestra petición sera Get pero podremos enviar un body
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );

		// Para que la peticion retorne un resultado y podamos manipularlo
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// Adjuntamos el json a nuestra petición
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

		// Agregamos los encabezados del contenido
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

		curl_setopt($ch, CURLOPT_HEADER, 0);

      	// Utilicen estas dos lineas si su petición es tipo https y estan en servidor de desarrollo
	 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		 
		// Ejecutamos la petición y almacenamos la respuesta en la variable $result
		$result = curl_exec($ch);

	 	// Se cierra el recurso CURL y se liberan los recursos del sistema
	 	curl_close($ch);

	 	$respuesta = json_decode($result, true);

	 	return $respuesta['documents'];
	}
/* --------------------------------------------------------------- CAMBIO DE CEDULAS ---------------------------------------------------------------- */

/* --------------------------------------------------------------- VALIDAR IMAGEN PACS ---------------------------------------------------------------- */

	/* Funcion que se encarga de buscar en la base de datos del PACS si la orden posee imagenes asociadas para validar si se el tecnico puede cerrar la orden desde el mediris. Esta funcion es llamada desde el mediris */
	function validarImagenPACS($localidad, $logs, $param = 0){

		$datos = explode("-", $localidad);

		$conf = new Config();
		$data = $conf->configSedes($datos[0]);

		$serverName = $data['serverName'];

		$ch = curl_init($serverName . $datos[1]);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
	 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		$result = curl_exec($ch);
	 	curl_close($ch);

	 	if($result > 0){
	 		$busqueda = $result;
			$status = "200";
	 	}else{
	 		$busqueda = $result;
			$status = "404";
	 	}

		if($param == 0){
			echo json_encode(array('status' => $status, 'busqueda' => $busqueda));
		}else{
			return array('status' => $status, 'busqueda' => $busqueda);
		}

	}

/* --------------------------------------------------------------- VALIDAR IMAGEN PACS ---------------------------------------------------------------- */

/* --------------------------------------------------------------- ENVIAR DATOS DE LA SOLICITUD ---------------------------------------------------------------- */
	
	/* Funcion que se encarga de buscar en la base de datos el correo del paciente y enviarlo junto con el numero de orden, email de la institucion y medico referido */
	function envioSolicitud($localidad, $logs){

		$user = "mediris";

		$datos = explode("-", $localidad);
		$localidad = $datos[0];
		$solicitud = $datos[1];

		$conf = new Config();
		$data = $conf->configSedes($localidad);

		$host = $data['host'];
		$pwd = $data['pwd'];
		$url = $data['url'];
		$user_agent = $data['user_agent'];
		$id_institucion = $data['id_institucion'];

		$mysqli = new mysqli($host,  $user, $pwd);
		$mysqli->set_charset("utf8");
		if ($mysqli->connect_errno) {
		    echo $mysqli->connect_error;
		    exit();
		}else{

			//$query = "SELECT T1.id, T1.patient_id, T2.id as orden, T2.procedure_id, T3.email, T3.cellphone_number, T3.last_name as apellido_paciente, T3.first_name as nombre_paciente, T4.first_name as nombre_referido, T4.last_name as apellido_referido, T4.email as email_referido FROM `apimeditron`.`service_requests` T1 LEFT JOIN `apimeditron`.`requested_procedures` T2 ON(T2.service_request_id = T1.id) LEFT JOIN `mediris`.`patients` T3 ON(T3.id = T1.patient_id) LEFT JOIN  `apimeditron`.`referrings` T4 ON(T4.id = T1.referring_id) WHERE T1.id = '".$solicitud."'";

			$query = "SELECT T1.id, T1.patient_id, T2.id as orden, T2.procedure_id, T3.email, T3.cellphone_number, T3.last_name as apellido_paciente, T3.first_name as nombre_paciente, T4.first_name as nombre_referido, T4.last_name as apellido_referido, T4.email as email_referido, T5.description as patient_type FROM `apimeditron`.`service_requests` T1 LEFT JOIN `apimeditron`.`requested_procedures` T2 ON(T2.service_request_id = T1.id) LEFT JOIN `mediris`.`patients` T3 ON(T3.id = T1.patient_id) LEFT JOIN  `apimeditron`.`referrings` T4 ON(T4.id = T1.referring_id) LEFT JOIN  `apimeditron`.`patient_types` T5 ON(T5.id = T1.patient_type_id) WHERE T1.id = '".$solicitud."'";

			$resultado = $mysqli->query($query);

			while ($datos = $resultado->fetch_array(MYSQLI_ASSOC)) {

				$phone = '';

				if($datos['cellphone_number']){
					$phone = substr($datos['cellphone_number'],1);
					$phone = '+58'.$phone;
				}

				//$data = array("accession_number" => $datos['orden'], "institution" => $id_institucion, "patient_email" => validateEmail($datos['email']), "email_ref_institution" => "", "email_ref_physician" => validateEmail($datos['email_referido']), "referred_physician" => $datos['nombre_referido'] . " " . $datos['apellido_referido'], "patient_phone" => $phone, "patient_name" => $datos['nombre_paciente'] . " " . $datos['apellido_paciente']);

				$data = array("accession_number" => $datos['orden'], "institution" => $id_institucion, "patient_email" => validateEmail($datos['email']), "email_ref_institution" => "", "email_ref_physician" => validateEmail($datos['email_referido']), "referred_physician" => $datos['nombre_referido'] . " " . $datos['apellido_referido'], "patient_phone" => $phone, "patient_name" => $datos['nombre_paciente'] . " " . $datos['apellido_paciente'], "study_type" => $datos['patient_type']);

				$response = enviarInforme($data, $user_agent);
			
				$logs->logsHandler('0009', "Respuesta Viewmed orden #: " . $datos['orden'] . " - " . $response ."\r\n\n----------------------------------------------------------------------------------------------------------------------------------- \r\n\n", "exito", "solicitud", $localidad);

				echo json_encode($response);
			}
		}
		
		$mysqli->close();
	}

/* --------------------------------------------------------------- ENVIAR DATOS DE LA SOLICITUD ---------------------------------------------------------------- */

/* --------------------------------------------------------------- ENVIAR DATOS DE LA SOLICITUD CMDLT CLC ---------------------------------------------------------------- */
	
	/* Funcion que se encarga de buscar en la base de datos el correo del paciente y enviarlo junto con el numero de orden, email de la institucion y medico referido CMDLT CLC*/
	/*function envioSolicitudTrinidadCLC($localidad, $logs){

		$user = 'root';
		$pwd = "017F22bed";
		
		if($localidad == '04'){
			$host = "192.168.9.2:3306";
			$user_agent = "Las_Ciencias";
			$id_institucion = "5fc044c3a2be3a1fa623faf2";
		}else{
			$host = "192.168.11.30:3306";
			$user_agent = "IDACA - CMDT";
			$id_institucion = "5f6cd01f9e80a3612d70ead4";
		}

		$mysqli = new mysqli($host,  $user, $pwd);
		$mysqli->set_charset("utf8");

		if($mysqli->connect_errno){
		    echo $mysqli->connect_error;
		    exit();
		}else{
			
			$fechaIni = date('Y') . "-" . date('m') . "-" . date('d') . " 00:00:00";
			$fechaFin = date('Y') . "-" . date('m') . "-" . date('d') . " 23:59:59";

			$query = "SELECT T1.id, T1.fecha_solicitud, T1.id_paciente, T2.id as orden, T2.costoRadiologo, T2.id_procedimiento, T3.email, T3.telefonoMovil as cellphone_number FROM veris2.solicitud T1 LEFT JOIN veris2.orden T2 ON (T2.id_solicitud = T1.id) LEFT JOIN veris2.paciente T3 ON (T3.id = T1.id_paciente) WHERE T1.fecha_solicitud BETWEEN '".$fechaIni."' AND '".$fechaFin."'";

			$resultado = $mysqli->query($query);

			while ($fila = $resultado->fetch_array(MYSQLI_ASSOC)) {

				if($fila['costoRadiologo'] == 0){

					$phone = '';

					if($fila['cellphone_number']){
						$phone = substr($fila['cellphone_number'],1);
						$phone = '+58'.$phone;
					}

					$data = array("accession_number" => $fila['orden'], "institution" => $id_institucion, "patient_email" => validateEmail($fila['email']), "email_ref_institution" => "", "email_ref_physician" => "", "patient_phone" => $phone);
					
					// echo json_encode($data) . " ";
					
					$response = enviarInforme($data, $user_agent);

					$logs->logsHandler('0010', "Respuesta Viewmed orden #: " . $fila['orden'] . " - " . $response ."\r\n\n----------------------------------------------------------------------------------------------------------------------------------- \r\n\n", "exito", "solicitud", $localidad);

					$respuesta = json_decode($response, true);

					if(!empty(json_encode($respuesta['documents'][0]['_id']))){
						$upd = "UPDATE `veris2`.`orden` SET `costoRadiologo` = '1' WHERE id = '".$fila['orden']."'";
						$resultadoupd = $mysqli->query($upd);
					}
					
					echo json_encode($response);
				}
			}
	
			$resultado->close();
		}

		
		$mysqli->close();

	}*/

/* --------------------------------------------------------------- ENVIAR DATOS DE LA SOLICITUD CMDLT CLC ---------------------------------------------------------------- */

/* --------------------------------------------------------------- CAMBIAR FECHA DE LA SOLICITUD ---------------------------------------------------------------- */
	
	/* Funcion que se encarga de buscar en la base de datos del mediris las solicitudes y ordenes asociadas de la fecha que se recibe por parametros, con el numero de orden se busca la fecha de creacion del estudio en el pacs y se compara con la fecha que recibe la funcion, si las fechas son diferentes entonces se actualiza la fecha de la solicitud en la base de datos del mediris*/
	/*function cambiarFechaEstudiosCMDLT($fecha, $logs){

		$user = "mediris";
		$host = "mediriscdt.idaca.com.ve:3306";
		$pwd = "jd9qq4WFB0mC1ZreOQ==";

		$mysqli = new mysqli($host,  $user, $pwd);
		$mysqli->set_charset("utf8");

		if ($mysqli->connect_errno) {
			$busqueda = 'No se pudo establecer la conexion con la BD del Mediris';
			$status = "500";
		    // echo $mysqli->connect_error;
		    // exit();
		}else{


			$query = "SELECT T1.id, T1.patient_identification_id, T2.id as orden FROM `apimeditron`.`service_requests` T1 LEFT JOIN `apimeditron`.`requested_procedures` T2 ON(T2.service_request_id = T1.id) WHERE T1.issue_date BETWEEN '".$fecha." 00:00:00' AND '".$fecha." 23:59:59'";

			$resultado = $mysqli->query($query) or die("Last error: {$con->error}\n");

			$ordenes = [];

			while ($datos = $resultado->fetch_array(MYSQLI_ASSOC)) {

				$fechaPacs = buscarFechaOrdenPACS($datos['orden'], $fecha);

		 		if($fecha != $fechaPacs){
				
					$query2 = "UPDATE `apimeditron`.`service_requests` SET issue_date = '".$fechaPacs." 06:00:00' WHERE id = '".$datos['id']."'";

					$resultado2 = $mysqli->query($query2);

					$query1 = "UPDATE `apimeditron`.`requested_procedures` SET technician_end_date = '".$fechaPacs." 06:00:00' WHERE id = '".$datos['orden']."'";

					$resultado1 = $mysqli->query($query1);
					array_push($ordenes, $datos['orden']);
					
					$logs->logsHandler('0011', "Se cambio la fecha a la orden #: " . $datos['orden'] . "\r\n\n----------------------------------------------------------------------------------------------------------------------------------- \r\n\n", "exito", "cambioFecha", 11);
				}
			}

			$resultado->close();

			$busqueda = $ordenes;
			$status = "200";
		}

		echo json_encode(array('status' => $status, 'busqueda' => $busqueda));
	}


	function buscarFechaOrdenPACS($orden, $fecha){
		

		$serverName = "192.168.11.17\MSSQLSERVER,1433";
		$connectionInfo = array( "Database"=>"opalrad",  "UID"=>"sa", "PWD"=>"1q2w3e4r5t");
		$conn = sqlsrv_connect( $serverName, $connectionInfo);

		$data = sqlsrv_query($conn, "SELECT * FROM STUDIES WHERE accession_number = '".$orden."'");
 		$row = sqlsrv_fetch_array($data, SQLSRV_FETCH_ASSOC);

		if($row){
	 		$fechaCompleta = json_decode(json_encode($row['study_datetime']))->date;
			$fechaPacs = explode(" ", $fechaCompleta)[0];
 		}else{
 			$fechaPacs = $fecha;
 		}

 		sqlsrv_close( $conn );

		return $fechaPacs;
	}*/

/* --------------------------------------------------------------- CAMBIAR FECHA DE LA SOLICITUD ---------------------------------------------------------------- */

/* --------------------------------------------------------------- SUBIR INFORMES POR FECHA ---------------------------------------------------------------- */
	
	/* Funcion que se encarga de buscar en la base de datos del mediris las solicitudes y ordenes asociadas de la fecha que se recibe por parametros, con el numero de orden se busca el informe y/o addendum y se suben a la plataforma de viewmed */
	function subirInformes($localidad, $logs){

		$datos = explode("-", $localidad);

		$localidad = $datos[0];
		$start_date = str_replace("_", "-", $datos[1]);
        $end_date = str_replace("_", "-", $datos[2]);
        $user = 'mediris';

        $conf = new Config();
		$data = $conf->configSedes($localidad);

		$host = $data['host'];
		$pwd = $data['pwd'];
		$url = $data['url'];
		$user_agent = $data['user_agent'];
		$id_institucion = $data['id_institucion'];

		$mysqli = new mysqli($host,  $user, $pwd);
		$mysqli->set_charset("utf8");

		if ($mysqli->connect_errno) {
			$busqueda = 'No se pudo establecer la conexion con la BD del Mediris';
			$status = "500";
		    // echo $mysqli->connect_error;
		    // exit();
		}else{

			// CONSULTO TODAS LAS ORDENES ENTRE EL RANGO DE FECHA Y QUE A SU VEZ TENGAN ESTATUS 6 SEAN DE LA INSTITUCION QUE REALIZO LA PETICION Y QUE NO SEAN CITAS YA QUE ESTAS NO GENERAN INFORMES
			//$query = "SELECT T1.id, T1.patient_identification_id, T2.id as orden FROM `apimeditron`.`service_requests` T1 JOIN `apimeditron`.`requested_procedures` T2 ON(T2.service_request_id = T1.id and T2.requested_procedure_status_id = '6') WHERE T1.issue_date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' and T1.institution_id = '".$data['id']."'";

			$query = "SELECT T1.id, T1.patient_identification_id, T2.id as orden, T2.procedure_id, T3.modality_id FROM `apimeditron`.`service_requests` T1 JOIN `apimeditron`.`requested_procedures` T2 ON(T2.service_request_id = T1.id and T2.requested_procedure_status_id = '6') JOIN `apimeditron`.`procedures` T3 ON(T3.id = T2.procedure_id) JOIN `apimeditron`.`modalities` T4 ON(T4.id = T3.modality_id and T4.active = 1 and T4.name <> 'CITA') WHERE T1.issue_date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' and T1.institution_id = '".$data['id']."'";

			$resultado = $mysqli->query($query) or die("Last error: {$con->error}\n");

			$ordenes = [];

			while ($datos = $resultado->fetch_array(MYSQLI_ASSOC)) {
				
				// VALIDAMOS SI EL INFORME TIENE ADDENDUM O ES EL INFORME ORIGINAL
				$datos['addendum'] = validarAddendums($datos['orden'], $mysqli);

				$queryInfo = "SELECT T1.service_request_id, T1.procedure_id, T1.text, T1.radiologist_user_name, T1.radiologist_user_id, T1.approve_user_name, T1.approve_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T2.height, T3.administrative_ID as sexo, T4.birth_date, T4.email, T4.cellphone_number, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T9.email as email_referido, T9.administrative_ID, T10.report_header, T10.report_footer FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN `mediris`.`sexes` T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN `mediris`.`patients` T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN `mediris`.`users` T7 ON(T7.id = T1.radiologist_user_id) LEFT JOIN `mediris`.`prefixes` T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN `mediris`.`institutions` T10 ON(T10.administrative_id = '".$localidad."') WHERE T1.id = '".$datos['orden']."'";

				$infoOrden = $mysqli->query($queryInfo);

				$dataOrden = $infoOrden->fetch_array(MYSQLI_ASSOC);

				if($datos['addendum']){

					$dataOrden['text'] = $datos['addendum'];

				}

				if(stripos($dataOrden['administrative_ID'], 'j') !== false || stripos($dataOrden['administrative_ID'], 'g') !== false){
					$referred_institution = $dataOrden['nombre_referido'] . " " . $dataOrden['apellido_referido'];
					$email_ref_institution = $dataOrden['email_referido'] == 'noespecificado@noespecificado.com' ? '' : validateEmail($dataOrden['email_referido']);
					$referred_physician = "";
					$email_ref_physician = "";
				}else{
					$referred_institution = "";
					$email_ref_institution = "";
					$referred_physician = $dataOrden['nombre_referido'] . " " . $dataOrden['apellido_referido'];
					$email_ref_physician = validateEmail($dataOrden['email_referido']);
				}

				$dataOrden['signature'] = str_replace("users","firmas",$dataOrden['signature']);


				$mimeHeader = obtenerMime($dataOrden['report_header'], $url);
				$headerB64 = obtenerB64($dataOrden['report_header'], $url);

				// mime de las imagenes (header, footer y firma del medico)
				$mimeFooter = obtenerMime($dataOrden['report_footer'], $url);
				$mimeSignature = obtenerMime($dataOrden['signature'], $url);

				// base64 de las imagenes (header, footer y firma del medico
				$footerB64 = obtenerB64($dataOrden['report_footer'], $url);
				$signatureB64 = obtenerB64($dataOrden['signature'], $url);

				$phone = '';

				if($dataOrden['cellphone_number']){
					$phone = substr($dataOrden['cellphone_number'],1);
					$phone = '+58'.$phone;
				}

				$dataViewmed = array("accession_number" => $datos['orden'], "institution" => $id_institucion, "patient_identification" => $dataOrden['patient_identification_id'], "patient_phone" => $phone, "patient_name" => $dataOrden['patient_first_name'] . " " . $dataOrden['patient_last_name'], "patient_gender" => $dataOrden['sexo'], "patient_birth" => $dataOrden['birth_date'] . ".000", "patient_email" => validateEmail($dataOrden['email']), "referred_institution" => $referred_institution, "email_ref_institution" => $email_ref_institution, "referred_physician" => $referred_physician, "email_ref_physician" => $email_ref_physician, "date_report" => $dataOrden['culmination_date'] . ".000", "attached" => [], "study_date" => $dataOrden['issue_date'] . ".000", "study_type" => $dataOrden['patient_type'], "study_name" => $dataOrden['procedimiento'], "physician_name" => $dataOrden['prexif'] . " " .$dataOrden['approve_user_name'], "physician_specialty" => $dataOrden['position'], "tag1" => array("Estatura" => $dataOrden['height'] . " m"), "tag2" => null, "tag3" => null, "report" => array("header" => array("mime" => $mimeHeader, "content" => $headerB64), "body" => array("mime" => 'text/html', "content" => $dataOrden['text']), "footer" => array("mime" => $mimeFooter, "content" => $footerB64), "signature" => array("mime" => $mimeSignature, "content" => $signatureB64)));

				$response = enviarInforme($dataViewmed, $user_agent);
				$respuesta = json_decode($response, true);

				if(!empty(json_encode($respuesta['documents'][0]['_id']))){

					array_push($ordenes, $datos['orden']);

					$logs->logsHandler('0012', "Respuesta Viewmed orden #: " . $datos['orden'] . " - " . $response ."\r\n\n----------------------------------------------------------------------------------------------------------------------------------- \r\n\n", "exito", "informesSubidos", $localidad);
				}
				$infoOrden->close();
			}

			$resultado->close();

			$busqueda = $ordenes;
			$status = "200";
		}

		echo json_encode(array('status' => $status, 'busqueda' => $busqueda, 'localidad' => $localidad));
	}

	function validarAddendums($orden, $mysqli){

        $sql = "SELECT * FROM `apimeditron`.`addendums` WHERE requested_procedure_id = '".$orden."' ORDER BY id DESC LIMIT 1";
        $resultado = $mysqli->query($sql) or die("Last error: {$con->error}\n");
        $addendum = $resultado->fetch_array(MYSQLI_ASSOC);

        return count($addendum) > 0 ? $addendum['text'] : '';
    }

    function obtenerMimeCMDLT($file, $url){

		$img = file_get_contents($url. "/" . $file);

		$fileArray = pathinfo($file);

		$nameImg = $fileArray['filename'];

		file_put_contents($nameImg, $img);

		$mime = mime_content_type($nameImg);

		unlink($nameImg);

		return $mime;
	}

	function obtenerB64CMDLT($file, $url){
		return base64_encode(file_get_contents($url. "/" . $file));
	}

/* --------------------------------------------------------------- SUBIR INFORMES POR FECHA ---------------------------------------------------------------- */

/* --------------------------------------------------------------- ASOCIAR PACIENTES ---------------------------------------------------------------- */
	
	/* Funcion que se encarga de buscar en la base de datos del mediris los pacientes "nuevos" y con la cedula de cada uno buscar en la tabla old_patients (migrados) si hay algun registro con esa cedula, en caso de encontrarlos le actualiza el campo id_patient_mediris con el id del usuario en la tabla patients para asociarlos */
	function asociarPacientes($localidad, $logs){

		$user = 'mediris';

		$conf = new Config();
		$data = $conf->configSedes($localidad);

		$host = $data['host'];
		$pwd = $data['pwd'];

		$mysqli = new mysqli($host,  $user, $pwd);
		$mysqli->set_charset("utf8");

		if ($mysqli->connect_errno) {
			$busqueda = 'No se pudo establecer la conexion con la BD del Mediris';
			$status = "500";
		    // echo $mysqli->connect_error;
		    // exit();
		}else{

			$query = "SELECT * FROM `mediris`.`patients`";
			$resultado = $mysqli->query($query) or die("Last error: {$con->error}\n");

			$pacientes = [];

			while ($datos = $resultado->fetch_array(MYSQLI_ASSOC)) {

				$queryInfo = "SELECT * FROM `mediris`.`old_records_pacientes` WHERE cedula = '".$datos['patient_ID']."'";
				$infoPaciente = $mysqli->query($queryInfo);
				$dataPaciente = $infoPaciente->fetch_array(MYSQLI_ASSOC);
				
				if($dataPaciente){

					$update = "UPDATE `mediris`.`old_records_pacientes` SET `id_patient_mediris` = '".$datos['id']."' WHERE id = '".$dataPaciente['id']."'";
					$resultadoupd = $mysqli->query($update);

					if($resultadoupd == 1){
						array_push($pacientes, $datos['id']);
						$logs->logsHandler('0013', "Paciente asociado ID Mediris: " . $datos['id'] . " ID OLD: " . $dataPaciente['id'] . " Cedula: " . $datos['patient_ID'] ."\r\n\n----------------------------------------------------------------------------------------------------------------------------------- \r\n\n", "exito", "pacientesAsociados", $localidad);
					}
				}
			}

			$resultado->close();
			$infoPaciente->close();

			$busqueda = $pacientes;
			$status = "200";
		}

		echo json_encode(array('status' => $status, 'busqueda' => $busqueda));
	}

/* --------------------------------------------------------------- ASOCIAR PACIENTES ---------------------------------------------------------------- */

/* ---------------------------------------------------------- FACTURACION FINES DE SEMANA Y FERIADOS ------------------------------------------------ */

	/* Funcion que se encarga de buscar en la base de datos del as400 las facturas correspondientes a los fines de semanas y dias feriados */
	function facturacionfinsemanaferiado($localidad, $connect){

		$datos = [];
		$registros_por_fecha = [];
		$localidades = ['04', '11', '12', '16', '19', '20', '21', '22', '23'];
		$fechas = explode("-", $localidad);
		$fecha_ini = formatearFecha($fechas[0]);
		$fecha_fin = formatearFecha($fechas[1]);
		$arrayFeriado = obtenerDiasFeriados($connect);

		foreach($localidades as $localidad){

			$facturas = $connect->select("IDASYSF.ID76FL86", "*", "ACCCIA = '14' AND ACCTRA = 'FACT' AND ADDLOF = '".$localidad."' AND ADDSTS <> 10 AND ADDFEF BETWEEN '".$fecha_ini."' AND '".$fecha_fin."'", "", "");

			foreach($facturas as $factura){
				$fecha = $factura['FCD_FEC_FACT'];
				$año_mes = date('Y-m', strtotime($fecha));

			    if (!isset($registros_por_fecha[$localidad][$año_mes])) {
			        $registros_por_fecha[$localidad][$año_mes] = [];
			    }

			    if(date('N', strtotime($factura['FCD_FEC_FACT'])) == '6' || date('N', strtotime($factura['FCD_FEC_FACT'])) == '7' ){
					array_push($registros_por_fecha[$localidad][$año_mes], $factura);
				}else{
					if(array_search($factura['FCD_FEC_FACT'], $arrayFeriado)){
						array_push($registros_por_fecha[$localidad][$año_mes], $factura);
					}
				}
			}
		}

		foreach($registros_por_fecha as $clave => $valor){
			$array_finsemana_feriado_por_fecha = [];

			foreach($valor as $fecha => $data){
				array_push($array_finsemana_feriado_por_fecha, array('fecha' => $fecha, 'periodo_formateado' => formatearPeriodo($fecha), 'cantidad_estudios' => count($data)));
			}

			array_push($datos, array('codigo_localidad' => $clave, 'nombre_localidad' => obtenerNombreLocalidad($clave), 'periodo' => $array_finsemana_feriado_por_fecha));
		}

		echo json_encode($datos);
	}


	function formatearFecha($fecha){
		return date("d.m.Y", strtotime(str_replace("_", "-", $fecha)));
	}


	function obtenerDiasFeriados($connect){

		$array = [];
		$diasFeriados = $connect->select("IDASYSF.ID12FP", "ADFFEC", "ADFFEC BETWEEN '01.01.".date("Y")."' AND '31.12.".date("Y")."'", "", "");

		foreach($diasFeriados as $feriado){
			array_push($array, $feriado['ADFFEC']);
		}

		return $array;
	}


	function obtenerNombreLocalidad($localidad){
		$conf = new Config();
		$data = $conf->configSedes($localidad);

		return $data['name'];
	}


	function formatearPeriodo($fecha){
		$fec = explode("-", $fecha);

		$meses_año = array('01' => 'ene','02' => 'feb','03' => 'mar','04' => 'abr','05' => 'may','06' => 'jun','07' => 'jul','08' => 'ago','09' => 'sep','10' => 'oct','11' => 'nov','12' => 'dic');

		return $meses_año[$fec[1]]."-".$fec[0];
	}

/* ---------------------------------------------------------- FACTURACION FINES DE SEMANA Y FERIADOS ------------------------------------------------ */


/* ---------------------------------------------------------- VALIDAR ORDENES FACTURADAS ------------------------------------------------ */

	/* Funcion que se encarga de buscar en la base de datos del as400 la información de las facturas relacionadas a una orden */
	function validarFacturacion($localidad, $connect){

		$datos = explode("-", $localidad);
		$localidad = $datos[0];
		$accession_number = str_pad($datos[1], 12, 0, STR_PAD_LEFT);

		$info_factura = $connect->select("IDASYSF.ID76FL86", "ADDNRF, ADDSTS", "ACCCIA = '14' AND ADDLOF = '".$localidad."' AND ADRCOD IN('OR', 'O2', 'O3') AND ARQVAL = '".$accession_number."' AND ARGTIP = 'S' AND ARGSER NOT LIKE '%-O%' AND ADDSTS <> 10", "", "");

		if(count($info_factura) > 0){

			$info_factura[0]['ADDSTS'] = $info_factura[0]['ADDSTS'] != '09' || $info_factura[0]['ADDSTS'] != '10' ? 'Emitida' : 'Anulada';

			$nro_factura = $info_factura[0]['ADDNRF'];
			$estatus_factura = $info_factura[0]['ADDSTS'];
			$estatus_info = 200;

		}else{
			$nro_factura = 0;
			$estatus_factura = 0;
			$estatus_info = 404;
		}

		echo json_encode(array("nro_factura" => $nro_factura, "estatus_factura" => $estatus_factura, "estatus_info" => $estatus_info));

	}
/* ---------------------------------------------------------- VALIDAR ORDENES FACTURADAS ------------------------------------------------ */


/* ---------------------------------------------------------- CAMBIAR IR DE INSTITUCION SERVICE REQUEST ------------------------------------------------ */

	/* Funcion que se encarga de cambiar el id de institucion en las solicitudes cuando es multisucursal el mediris. Recibe el id administrativo de la peticion y busca el id autoincremental en la tabla y es el que asocia a las solicitudes */
	function cambiarInstitucion($localidad, $logs){

		$datos = explode("-", $localidad);

		$conf = new Config();
		$data = $conf->configSedes($datos[0]);

		$host = $data['host'];
		$pwd = $data['pwd'];

		$mysqli = new mysqli($host,  'mediris', $pwd);
		$mysqli->set_charset("utf8");

		if ($mysqli->connect_errno) {
			$busqueda = 'No se pudo establecer la conexion con la BD del Mediris';
			$status = "500";
		    // echo $mysqli->connect_error;
		    // exit();
		}else{

			$sqlModality = "SELECT id FROM apimeditron.modalities where name = '".$datos[1]."'";
			$resulModality = $mysqli->query($sqlModality) or die("Last error: {$con->error}\n");
			$modalidad = $resulModality->fetch_array(MYSQLI_ASSOC);


			$sqlProcedure = "SELECT id FROM apimeditron.procedures where modality_id = '".$modalidad['id']."'";
			$resulProcedure = $mysqli->query($sqlProcedure) or die("Last error: {$con->error}\n");

			while ($datos = $resulProcedure->fetch_array(MYSQLI_ASSOC)) {

				$sql = "SELECT T1.service_request_id, T2.id, T2.institution_id FROM apimeditron.requested_procedures T1 JOIN apimeditron.service_requests T2 ON(T2.id = T1.service_request_id) where T1.procedure_id = '".$datos['id']."'";
				$resultado = $mysqli->query($sql) or die("Last error: {$con->error}\n");

				while ($datos1 = $resultado->fetch_array(MYSQLI_ASSOC)) {

					$upd = "UPDATE `apimeditron`.`service_requests` SET `institution_id` = '".$data['id']."' WHERE id = '".$datos1['id']."'";
					$resultadoupd = $mysqli->query($upd);
				}

			}
		}
	}
/* ---------------------------------------------------------- CAMBIAR IR DE INSTITUCION SERVICE REQUEST ------------------------------------------------ */

?>