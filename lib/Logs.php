<?php 

/**
* @author Billy Gonzalez
* @version 1.0 13/09/2022
*/

class Logs{

	public function __construct(){

	}

	/**
	 * [logsHandler - Guarda todos los movimientos en un log TXT]
	 * @param  [string] $cod [Codigo de la transaccion]
	 * @param  [string] $message  [Mensaje de la transaccion]
	 * @param  [string] $tipo  [Tipo de transaccion]
	 * @param  [string] $mov  [Funcion que fue llamada del webservice]
	 * @param  [string] $loc  [localidad]
	 */
	public function logsHandler($cod,$message,$tipo, $mov, $loc){
		date_default_timezone_set('America/Caracas');
		if($mov == "aprobados"){
			$filename = 'log/'.$loc.'/logs_'.$mov.'_'.date('Ym').'.log';
		}else{
			$filename = 'log/'.$loc.'/logs_'.$mov.'_'.date('Ym').'.log';
		}
		$dirname = dirname($filename);
		if(!is_dir($dirname)){
			mkdir($dirname, 0755, true);
		}
		$file = fopen($filename,'a');
		if($tipo == "exito"){
			fwrite($file,"[".date("r")."] PROCESO $cod: $message\r\n\n");
		}else{
			fwrite($file,"[".date("r")."] ERROR $cod: $message\r\n\n");
		}
		fclose($file);
	}
}
?>