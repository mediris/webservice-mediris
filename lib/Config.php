<?php 

/**
* @author Billy Gonzalez
* @version 1.0 17/10/2022
*/

class Config{

	public function configSedes($loc){

		/**
		* 04 - CLC
		* 11 - CMDLT
		* 12 - CMDLT MNU
		* 16 - Oasis
		* 19 - Sanatrix
		* 20 - La Floresta
		* 21 - Fenix
		* 22 - La Arboleda
		* 23 - Higuerote
		* 25 - La Viña
		* 99 - DEV
		**/

		
		$sedes = array(
			'04' => 
				array('name' => 'Las Ciencias', 'host' => 'medirisclc.idaca.com.ve:3306', 'pwd' => 'XLQ1WmIa4BU6XPUEiQ==', 'url' => 'https://medirisclc.idaca.com.ve/', 'user_agent' => 'Las_Ciencias', 'id_institucion' => '5fc044c3a2be3a1fa623faf2', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc2NsYy5pZGFjYS5jb20udmUiLCJpYXQiOjE2NDk4NjA0NjQsIm5iZiI6MTY0OTg2MDQ2NCwianRpIjoiMDJSYmhTbmNiNnRqbHlEbSIsInN1YiI6MiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.snOHP6Oymd4-XYTXzI0aBNUEX3czt99KDvw4MC6tIP4', 'serverName' => 'http://192.168.9.1:9668/study_query/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'11' => 
				array('name' => 'CMDLT', 'host' => 'mediriscdt.idaca.com.ve:3306', 'pwd' => 'jd9qq4WFB0mC1ZreOQ==', 'url' => 'https://mediriscdt.idaca.com.ve/', 'user_agent' => 'IDACA - CMDT', 'id_institucion' => '5f6cd01f9e80a3612d70ead4', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc2NtZGx0LmlkYWNhLmNvbS52ZSIsImlhdCI6MTY0MjUxNjk4MCwibmJmIjoxNjQyNTE2OTgwLCJqdGkiOiJkSnpUOFhYdXlFNUlQOWdKIiwic3ViIjoyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.CASklLAk5NVhZJvbREYexIqViBlrh5igIyTvgWoyb2E', 'serverName' => 'http://192.168.11.17:9668/study_query/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'12' => 
				array('name' => 'CMDLT - MNU', 'host' => 'medirismnu.idaca.com.ve:3306', 'pwd' => 'DxTqz7HfRT1x6Tgx5w==', 'url' => 'https://medirismnu.idaca.com.ve/', 'user_agent' => 'CMDLT-MN', 'id_institucion' => '5f6cd01f9e80a3612d70ead4', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc21udS5pZGFjYS5jb20udmUiLCJpYXQiOjE2MzU5NDU4NTUsIm5iZiI6MTYzNTk0NTg1NSwianRpIjoidkxhd3ZHUkpCM1JRWXRqTSIsInN1YiI6MiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.20wMYpoFX8CX99u0xPfTp1uCKTMQtPNSbjh0d0sHkLs', 'serverName' => 'http://192.168.11.17:9668/study_query/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'16' => 
				array('name' => 'Oasis',  'host' => 'medirisoas.idaca.com.ve:3306', 'pwd' => 'VG71hHYt7VudHEZvXQ==', 'url' => 'https://medirisoas.idaca.com.ve/', 'user_agent' => 'Oasis', 'id_institucion' => '5fc04463a2be3a1fa623faf1', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc29hcy5pZGFjYS5jb20udmUiLCJpYXQiOjE2NDY5MjI3OTYsIm5iZiI6MTY0NjkyMjc5NiwianRpIjoiNE4zbkZ0ZnZFbTM1NVJKMiIsInN1YiI6MiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.qDMxrrAnXr_CkFeOES9qzs0cIogMHGCX-wAQvRnLzT8', 'serverName' => 'http://192.168.18.2:9668/mirth_studyquery/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'19' => 
				array('name' => 'Sanatrix', 'host' => 'medirissnx.idaca.com.ve:3306', 'pwd' => 'DRguVSRmOCUg0z8ZQ==', 'url' => 'https://medirissnx.idaca.com.ve/', 'user_agent' => 'IDACA - Sanatrix', 'id_institucion' => '5f6a175e9807fc0c4f63600d', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc3NueC5pZGFjYS5jb20udmUiLCJpYXQiOjE2NDQ3MDQ2NDgsIm5iZiI6MTY0NDcwNDY0OCwianRpIjoiS0RaTHRDQU0yYXduQmNPSiIsInN1YiI6MiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.SSrJdILZSng01cvb2nvocTGuTo5YjYj0PLX7xhO6ZdA', 'serverName' => 'http://192.168.23.6:9668/study_query/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'20' => 
				array('name' => 'La Floresta', 'host' => 'medirisflt.idaca.com.ve:3306', 'pwd' => 'n68Ui3t1K51UBon6g==', 'url' => 'https://medirisflt.idaca.com.ve/', 'user_agent' => 'IDACA - La Floresta', 'id_institucion' => '5e441a69a045161b2515c27f', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc2ZsdC5pZGFjYS5jb20udmUiLCJpYXQiOjE2NDI3MDk4OTEsIm5iZiI6MTY0MjcwOTg5MiwianRpIjoiRkNkZGVTOUl4NkdHYmxZaSIsInN1YiI6MiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.RrhOLO3eJOa-UA0QL84dhFrDI-1ViCOjlA6QtssEA-c', 'serverName' => 'http://192.168.24.6:9668/mirth_studyquery/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'21' => 
				array('name' => 'Fénix', 'host' => 'medirisfnx.idaca.com.ve:3306', 'pwd' => 'B7U1I6MLZXssSK2WYg==', 'url' => 'https://medirisfnx.idaca.com.ve/', 'user_agent' => 'fenix_salud', 'id_institucion' => '612e984a68541f7c7b8aef22', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc2ZueC5pZGFjYS5jb20udmUiLCJpYXQiOjE2NDQ3MTE3ODIsIm5iZiI6MTY0NDcxMTc4MiwianRpIjoiaXQ4Y3ZWSnFLcHZIeXRLTSIsInN1YiI6MiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.0_0OYn3Gp76r9Ks-X9KNNtrubpOiOnBgl-KYbTb8P60', 'serverName' => 'http://192.168.25.6:9668/study_query/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'22' => 
				array('name' => 'La Arboleda', 'host' => 'medirisabl.idaca.com.ve:3306', 'pwd' => 'vDF3BK5DLYk018GVg==', 'url' => 'https://medirisabl.idaca.com.ve/', 'user_agent' => 'Arboleda', 'id_institucion' => '61155f23bf18d14a9a5db36e', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc2FibC5pZGFjYS5jb20udmUiLCJpYXQiOjE2NDczODU1NzEsIm5iZiI6MTY0NzM4NTU3MSwianRpIjoiakZCRXRrdG9yTFpNMjk0OCIsInN1YiI6MiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.pKvHyipbs_aZWVWpeS1pF1ayh_hS5-Bwq5FONwh0_BI', 'serverName' => 'http://192.168.26.6:9668/study_query/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'23' => 
				array('name' => 'Higuerote', 'host' => 'medirishig.idaca.com.ve:3306', 'pwd' => 'Ow2ddRiFZK8izMDLNw==', 'url' => 'https://medirishig.idaca.com.ve/', 'user_agent' => 'idaca-higuerote', 'id_institucion' => '61ad0663b08e6a15ac7d659f', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaXJpc2hpZy5pZGFjYS5jb20udmUiLCJpYXQiOjE2NDIxMjg0MzEsIm5iZiI6MTY0MjEyODQzMSwianRpIjoiYWxIN201Y0MyYzdKUFl6NyIsInN1YiI6MiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.eHUBnA8CP-AwyKTYSBLdjUcuPWzrhiUbz8f8_1qHd9A', 'serverName' => 'http://192.168.20.6:9668/study_query/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'25' => 
				array('name' => 'La Viña', 'host' => 'mediriscpv.idaca.com.ve:3306', 'pwd' => 'W5X34oK5L9FIG0NGDw==', 'url' => 'https://mediriscpv.idaca.com.ve/', 'user_agent' => 'idaca-laviña', 'id_institucion' => '670296968bc4da38a15d4f1d', 'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbWVkaWRldi5tZWRpdHJvbi5jb20udmUiLCJpYXQiOjE3MDk5MDI4NDYsIm5iZiI6MTcwOTkwMjg0NiwianRpIjoiTUl2eHNpRlVvOFl4UTRWayIsInN1YiI6MiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.3jVVoNivO17uj9HOvoWNcs75rHKLDySEcT0P0CDQIqY', 'serverName' => 'http://192.168.28.6:9668/study_query/?accession_no=', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),
			'99' => 
				array('name' => 'DEV', 'host' => 'medirisdev.meditron.com.ve:3306', 'pwd' => 'aMHLonAK6WQv9YSQg==', 'url' => 'https://medirisdev.meditron.com.ve', 'user_agent' => 'IDACA - DEV', 'id_institucion' => '5e441a69a045161b2515c27f', 'token' => '', 'serverName' => '', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1'),

			'4545' => 
				array('name' => 'DEV', 'host' => 'medirisdev.meditron.com.ve:3306', 'pwd' => 'aMHLonAK6WQv9YSQg==', 'url' => 'https://medirisdev.meditron.com.ve', 'user_agent' => 'IDACA - DEV', 'id_institucion' => '5e441a69a045161b2515c27f', 'token' => '', 'serverName' => '', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '4'),

			'30' => 
				array('name' => 'VIEWMED DEV', 'host' => 'demoviewmed.meditron.com.ve:3306', 'pwd' => 'FghlraWUqsdwbB48A==', 'url' => 'https://demoviewmed.meditron.com.ve', 'user_agent' => 'VIEWMED - DEV', 'id_institucion' => '5e441a69a045161b2515c27f', 'token' => '', 'serverName' => '', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '2'),

			'31' => 
				array('name' => 'VIEWMED DEV', 'host' => 'demoviewmed.meditron.com.ve:3306', 'pwd' => 'FghlraWUqsdwbB48A==', 'url' => 'https://demoviewmed.meditron.com.ve', 'user_agent' => 'VIEWMED - DEV', 'id_institucion' => '5e441a69a045161b2515c27f', 'token' => '', 'serverName' => '', 'connectionInfo' => array('Database' => '', 'UID' => '', 'PWD' => ''), 'id'=> '1')
		);

		return $sedes[$loc];
	}

}

?>