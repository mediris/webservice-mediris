<?php 

/**
* @author Johan Davila <j.davila@meditron.com.ve>
* @author Luis Ramos <luisenrique545@gmail.com>
* @version 1.0 03/01/2017					(dd-mm-yyyy)
*
* La clase Connections conecta con la base de datos
* y ejecuta operaciones SQL para retornar datos.
*/

class Connections{

   private $Usuario;

   private $Contrasena;

   private $Cid;

   private $dsn;

	public function __construct($usuario, $contrasena, $baseDatos){

		$this->Usuario= $usuario;

		$this->Contrasena= $contrasena;

		$this->dsn="odbc:" . $baseDatos;

		$this->Cid= $this->connect();
	}
	
	public function connect(){

		try{
			$dbh = new PDO($this->dsn, $this->Usuario, $this->Contrasena, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
		catch(PDOException $ex){
			$this->logsHandler('020','NO SE PUDO CONECTAR CON LA BASE DE DATOS -  '.$ex, "error", "", "");
			die();
		}

		return $dbh;
	}	

	public function select($table, $select, $where, $group, $order){

		$sql = 'SELECT '.$select.' FROM '.$table;

		!empty($where)?$sql .= ' WHERE '.$where:$sql .='';

		!empty($group)?$sql .= ' GROUP BY '.$group:$sql .='';

		!empty($order)?$sql .= ' ORDER BY '.$order:$sql .='';

		//echo $sql;

		//$stmt = $this->Cid->prepare($sql);

		try{

			$stmt = $this->Cid->prepare($sql);
			$stmt->execute();

		}catch(PDOException $ex){

			$errorResponse = $ex.' | SQL: '.$sql;

			$this->logsHandler('010', $errorResponse, "error", "99");
		}

		//Convierte el result en un array
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//Convierte los resultados del Array en UTF-8
	    array_walk_recursive($result, function(&$item, $key){
	        if(!mb_detect_encoding($item, 'utf-8', true)){
	                $item = utf8_encode($item);
	        }
	    });

		//$result = array_map("utf8_encode", $arreglo);
		//var_dump($result);
		return $result;
	}


	public function update($table, $col_val, $where){

		$sql = 'UPDATE '.$table.' SET '.$col_val;

		!empty($where)?$sql .= ' WHERE '.$where.'':$sql .='';

		//echo $sql;

		$stmt = $this->Cid->prepare($sql);

		try{

			$stmt->execute();

		}catch(PDOException $ex){	

			$errorResponse = $ex.' | SQL: '.$sql;

			$this->logsHandler('012', $errorResponse, "error", "");
		}
	}

	public function delete($table, $where){

		$sql = 'DELETE FROM '.$table;

		!empty($where)?$sql .= ' WHERE '.$where.'':$sql .='';

		//echo $sql;

		$stmt = $this->Cid->prepare($sql);

		try{
			 $stmt->execute();

		}catch(PDOException $ex){

			$errorResponse = $ex.' | SQL: '.$sql;

			$this->logsHandler('013', $errorResponse, "error", "");

			die();
		}

		return $stmt;
	}

	public function insert($table, $column, $value){

		$sql = 'INSERT INTO '.$table.' ('.$column.') 

			VALUES ('.$value.')';

		//echo $sql;

		$stmt = $this->Cid->prepare($sql);

		try{

			//$stmt = $this->Cid->prepare($sql);

			$stmt->execute();

			$response = true;

			return $response;

		}catch(PDOException $ex){
			$errorResponse = $ex.' | SQL: '.$sql;

			$this->logsHandler('011', $errorResponse, "error", "");

			$response = false;

			return $response;
		}
	}

	/**
	 * [queryAll - Recibe el QUERY completo para ejecutar]
	 * @param  [string] $query     [Querya completo]
	 * @param  [integer] $fetchType [1 = para nombres asociativos (FETCH_ASSOC), 2 = para nombres numeros (FETCH_NUM)]
	 * @return [array]            [Data de la DB]
	 */
	public function queryAll($query, $fetchType){

		$sql = $query;

		//echo $sql;

		$stmt = $this->Cid->prepare($sql);

		try{

			$stmt->execute();

		}catch(PDOException $ex){

			$errorResponse = $ex.' | SQL: '.$sql;

			$this->logsHandler('002', $errorResponse, "error", "");
		}

		//Convierte el result en un array
		if ($fetchType == 1) {
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		} elseif ($fetchType == 2) {
			$result = $stmt->fetchAll(PDO::FETCH_NUM); 
		}

		//Convierte los resultados del Array en UTF-8
	    array_walk_recursive($result, function(&$item, $key){
	        if(!mb_detect_encoding($item, 'utf-8', true)){
	                $item = utf8_encode($item);
	        }
	    });

		return $result;
	}

	/**
	 * [logsHandler - Guarda todos los movimientos en un log TXT]
	 * @param  [string] $cod [Codigo de la transaccion]
	 * @param  [string] $message  [Mensaje de la transaccion]
	 * @param  [string] $tipo  [Tipo de transaccion]
	 * @param  [string] $mov  [Funcion que fue llamada del webservice]
	 * @param  [string] $loc  [localidad]
	 */
	public function logsHandler($cod,$message,$tipo, $mov, $loc){
		date_default_timezone_set('America/Caracas');
		if($mov == "aprobados"){
			$filename = 'log/'.$loc.'/logs_'.$mov.'_'.date('Ym').'.log';
		}else{
			$filename = 'log/'.$loc.'/logs_'.$mov.'_'.date('Ym').'.log';
		}
		$dirname = dirname($filename);
		if(!is_dir($dirname)){
			mkdir($dirname, 0755, true);
		}
		$file = fopen($filename,'a');
		if($tipo == "exito"){
			fwrite($file,"[".date("r")."] PROCESO $cod: $message\r\n\n");
		}else{
			fwrite($file,"[".date("r")."] ERROR $cod: $message\r\n\n");
		}
		fclose($file);
	}
}
?>